#!/bin/sh
#This has been tested for Mac OSX but not Windows bash or Linux.
RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

sshfolder="`readlink -f ~/.ssh`"
echo ""
echo "This script will generate two new public and private ssh keys to be used to connect to Bitbucket and Gitlab."
echo ""
echo "The file names can be seen below:"
echo "Bitbucket     -       $USER-bitbucket-siemens"
echo "Gitlab        -       $USER-gitlab-siemens"
echo "They will be copied to $sshfolder."
echo ""
echo "Checking for conflicting files."

bitbucketkeyfound=$(find ~/.ssh -name $USER-bitbucket-siemens*)
gitlabkeyfound=$(find ~/.ssh -name $USER-gitlab-siemens*)

echo "$bitbucketkeyfound"
echo "$gitlabkeyfound"
echo ""
read -p "If there are no conflicts found press enter to continue. If conflicts are found, please rename conflicting files."
echo ""
echo ""
echo "Creating Bitbucket ssh key pair..."
ssh-keygen -t rsa -N "" -f ~/.ssh/$USER-bitbucket-siemens
echo ""
echo "Creating Gitlab ssh key pair..."
ssh-keygen -t rsa -N "" -f ~/.ssh/$USER-gitlab-siemens
echo ""

bitbucketfound=$(grep -i 'bitbucket' $sshfolder/config)
gitlabfound=$(grep -i 'gitlab' $sshfolder/config)

if [ -z "$bitbucketfound" ]
    then
      echo "${GREEN}ALL CLEAR - No bitbucket entry found.${NC}"
    else
      echo "${YELLOW}- WARNING -${NC}"
      echo "Bitbucket entry found. Please delete the section in $sshfolder/config before continuing."
      read -p "Press enter to continue."
      
fi

echo ""

if [ -z "$gitlabfound" ]
    then
      echo "${GREEN}ALL CLEAR - No bitbucket entry found.${NC}"
    else
      echo "${YELLOW}- WARNING -${NC}"
      echo "Gitlab entry found. Please delete the section in $sshfolder/config before continuing."
      read -p "Press enter to continue."
fi


echo ""
echo "#Bitbucket ssh config created on `date`" >> config-temp.txt
echo "Host bitbucket.org" >> config-temp.txt
echo "     StrictHostKeyChecking accept-new" >> config-temp.txt
echo "     AddKeysToAgent yes" >> config-temp.txt
echo "     UseKeychain yes" >> config-temp.txt
echo "     IdentityFile ~/.ssh/$USER-bitbucket-siemens" >> config-temp.txt
echo "" >> config-temp.txt
echo "#Gitlab ssh config created on `date`" >> config-temp.txt
echo "Host gitlab.com" >> config-temp.txt
echo "     StrictHostKeyChecking accept-new" >> config-temp.txt
echo "     AddKeysToAgent yes" >> config-temp.txt
echo "     UseKeychain yes" >> config-temp.txt
echo "     IdentityFile ~/.ssh/$USER-gitlab-siemens" >> config-temp.txt
echo "" >> config-temp.txt


cat config-temp.txt ~/.ssh/config > config
cat config
cp config ~/.ssh
rm config
rm config-temp.txt

echo "Update of $sshfolder/config complete."

echo ""
echo "Please copy/pasta the following into the SSH-KEY section of Bitbucket - Found here: https://bitbucket.org/account/settings/ssh-keys/"
echo ""
cat "$sshfolder/$USER-bitbucket-siemens.pub"
echo ""
read -p "Press enter to continue."

echo ""
echo "Please copy/pasta into the SSH-KEY section of Gitlab - Found here: https://gitlab.com/-/profile/keys"
echo ""
cat "$sshfolder/$USER-gitlab-siemens.pub"
echo ""
read -p "Press enter to continue."

echo ""
echo "Testing connection to Bitbucket." 
ssh -Tv git@bitbucket.org
if [ $? -eq 0 ]
    then
        echo "${GREEN}SUCCESSFULLY CONNECTED TO BITBUCKET!${NC}"
    else
        echo "${RED}DRATS! SOMETHING WENT WRONG!${NC}"
fi

echo ""
echo "Testing connection to Gitlab." 
ssh -Tv git@gitlab.com
if [ $? -eq 0 ]
    then
        echo "${GREEN}SUCCESSFULLY CONNECTED TO GITLAB!${NC}"
    else
        echo "${RED}DRATS! SOMETHING WENT WRONG!${NC}"
fi
